import socket
import time
import datetime as dt

class Server_tcp(object):
    host = None
    port = None
    data = None
    s = None
    c = None
    addr = None
    def __init__(self, _host, _port):
        self.host = _host
        self.port = _port
        self.s = socket.socket()
        self.s.bind((_host, _port))

    def get_time_data_pars_str(self):
        dattim = dt.datetime.now()
        dattim = str(dattim).replace(" ",":")
        dattim = str(dattim).replace(".",":")
        dattim = str(dattim).replace("-",":")
        dattim = "_"+dattim
        return dattim + "\n"

    def server_listen(self, _max_cli):
        self.s.listen(_max_cli)

    def server_accept(self):
        self.c, self.addr = self.s.accept()
        #print ("Connected " + str(self.addr))

    def server_close(self):
        self.s.close()
        #print ("svr_cls")

    def server_send(self, data):
        try:
            self.c.sendall(str.encode(data))#self.get_time_data_pars_str())
        except Exception:
            #pass
            print("send error")

    def server_rcv(self):
        try:
            self.c.settimeout(15.0)
            data = self.c.recv(1024)
            return data.decode()
        except Exception:
            return "error"

    def get_cli(self):
        return self.c

    def close_cli(self):
        self.c.close()

    def set_cli(self, cli):
        self.c = cli
"""
server = Server_tcp("127.0.0.1", 5110)
server.server_listen()
while True:
    server.server_send()
"""
"""
def get_time_data_pars_str():
    dattim = dt.datetime.now()
    dattim = str(dattim).replace(" ",":")
    dattim = str(dattim).replace(".",":")
    dattim = str(dattim).replace("-",":")
    dattim = "_"+dattim
    return dattim + "\n"

def Main():
    host = '127.0.0.1'
    port = 5050

    s = socket.socket()
    s.bind((host, port))

    s.listen(1)
    c, addr = s.accept()
    print ("Connection from: " + str(addr))
    while True:
        data = c.recv(1024)
        if not data:
            break;
        print ("From client: " + str(data))
        data = str(data).upper()
        print ("To client: " + str(data))
        #c.send(data)
        c.send(get_time_data_pars_str())
        #time.sleep(2)
    c.close()

if __name__ == '__main__':
    Main()
"""

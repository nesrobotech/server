import sys
sys.path.append("../rtk")

from pyproj import Geod
#target - cp
#marker - marker

def get_ap_lon_lat():
    ap_llh_file = "../rtk/AP/AP.txt"
    #ap_llh_file = "rtk/AP/AP.txt"
    ap_f = open(ap_llh_file,"r")
    ap_f_data = str(ap_f.read()).split(':')
    ap_lat=float(ap_f_data[1])
    ap_lon=float(ap_f_data[2])
    ap_f.close()
    return ap_lon, ap_lat

def get_cp_lon_lat():
    cp_llh_file = "../rtk/CP/CP.txt"
    #cp_llh_file = "rtk/CP/CP.txt"
    cp_f = open(cp_llh_file,"r")
    cp_f_data = str(cp_f.read()).split(':')
    cp_lat=float(cp_f_data[1])
    cp_lon=float(cp_f_data[2])
    cp_f.close()
    return cp_lon, cp_lat

def get_azi_ap():
    g = Geod(ellps='WGS84')
    lon1, lat1 = get_cp_lon_lat()
    lon2, lat2 = get_ap_lon_lat()
    azi,az21,dst = g.inv(lon1, lat1,lon2, lat2)
    return azi

def measure(target_lat,target_lon,marker_lat,marker_lon):
    g = Geod(ellps='WGS84')
    azi,az21,dst = g.inv(target_lon,target_lat,marker_lon,marker_lat)
    return azi, dst

def get_azi_marker_local(marker_lat,marker_lon):
    azi_ap = get_azi_ap()
    lon1, lat1 = get_cp_lon_lat()
    azi_marker, dest = measure(lat1, lon1, marker_lat, marker_lon)
    azi_marker_local = azi_marker - azi_ap
    if azi_marker_local<0:
        azi_marker_local = azi_marker_local+360.0
    return azi_marker_local,dest


"""

target_lat = 55.34
target_lon = 33.10
marker_lat = 48.23
marker_lon = 34.15
a, d = measure(target_lat,target_lon,marker_lat,marker_lon)

print("Azi1: ", round(a,3), " dist1: ", round((d),3))"""

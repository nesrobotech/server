# -*- coding: utf-8 -*-
#ToDo Replace HTTP Status Codes acconding to the https://opensource.zalando.com/restful-api-guidelines/#150

from datetime import datetime


MARKER = {}


def get_timestamp():
    return datetime.now().strftime("%Y-%m-%d %H:%M:%S")


def create(flight_id, target_id, marker_id):
    """marker_create

    Create new marker for specified &#x60;flight_id&#x60; &amp; &#x60;target_id&#x60; #

    :param flight_id: Flight identifier
    :type flight_id: str
    :param target_id: Target identifier
    :type target_id: str
    :param marker_id: Marker identifier
    :type marker_id: str
    """
    marker_found = False
    if flight_id not in MARKER:
        MARKER[flight_id] = {}
    if target_id not in MARKER[flight_id]:
        MARKER[flight_id][target_id] = {}
        MARKER[flight_id][target_id]['marker'] = []
    for i in range(len(MARKER[flight_id][target_id]['marker']), 0, -1):
        if marker_id == MARKER[flight_id][target_id]['marker'][i - 1]['marker_id']:
            marker_found = True
    if not marker_found:
        MARKER[flight_id][target_id]['marker'].append({
            'marker_id': marker_id,
            "state": "n/a",
            "azimuth": 0,
            "distance": 0,
            "llh": {
                "lat": 0,
                "lon": 0,
                "hgt": 0
            },
            'timestamp': get_timestamp()
        })
        return {"status": "Ok"}, 200
    else:
        return {"status": "Error",
                "message": "Marker {marker_id} already exists".format(marker_id=marker_id)}, 400


def readlist(flight_id, target_id, lastsync=None):
    """       marker_readlist
        Read the list of markers for specified &#x60;flight_id&#x60; &amp; &#x60;target_id&#x60;
        :param flight_id: Flight identifier
        :type flight_id: str
        :param target_id: Target identifier
        :type target_id: str
        :param lastsync: Last sync timestamp. Format “%Y-%m-%d %H:%M:%S"
        :type lastsync: str"""
    if flight_id in MARKER and flight_id is not None:
        if target_id in MARKER[flight_id]:
            markerlist = MARKER.get(flight_id).get(target_id).get('marker')
            if lastsync is not None:
                try:
                    datetime.strptime(lastsync, "%Y-%m-%d %H:%M:%S")
                except ValueError:
                    return {"status": "Error",
                            "message": "lastsync format error"}, 400
                flist = []
                for i in range(len(markerlist), 0, -1):
                    if lastsync < markerlist[i - 1]['timestamp']:
                       flist.append(markerlist[i - 1])
                    else:
                        break
                return {"status": "Ok",
                        "marker": flist}, 200
            else:
                return {"status": "Ok",
                        "marker": markerlist}, 200
        else:
            return {"status": "Error",
                    "message": "Target {target_id} not exists".format(target_id=target_id)}, 400
    else:
        return {"status": "Error",
                "message": "Flight {flight_id} not exists".format(flight_id=flight_id)}, 400


def delall(flight_id, target_id):
    """       marker_delall
        Delete all markers for specified &#x60;flight_id&#x60; &amp; &#x60;target_id&#x60;
        :param flight_id: Flight identifier
        :type flight_id: str
        :param target_id: Target identifier
        :type target_id: str"""
    if flight_id in MARKER and flight_id is not None:
        if target_id in MARKER[flight_id]:
            del MARKER[flight_id][target_id]
            clean_markers = Popen("rm -rf ../rtk/MARKERS",bufsize=0,shell=True,stdout=None)
            clean_markers.wait()
            Popen("mkdir ../rtk/MARKERS",bufsize=0,shell=True,stdout=None)
            return {"status": "Ok"}, 200
        else:
            return {"status": "Error",
                    "message": "Target {target_id} not exists".format(target_id=target_id)}, 404
    else:
        return {"status": "Error",
                "message": "Flight {flight_id} not exists".format(flight_id=flight_id)}, 404

def state(flight_id, target_id, marker_id):
    """marker_state

    Read the state of the marker for specified &#x60;marker_id&#x60;

    :param flight_id: Flight identifier
    :type flight_id: str
    :param target_id: Target identifier
    :type target_id: str
    :param marker_id: Marker identifier
    :type marker_id: str
    """
    if flight_id in MARKER and flight_id is not None:
        if target_id in MARKER[flight_id]:
            for i in range(len(MARKER[flight_id][target_id]['marker']), 0, -1):
                if marker_id == MARKER[flight_id][target_id]['marker'][i - 1]['marker_id']:
                    pos = MARKER[flight_id][target_id]['marker'][i - 1]['llh']
                    state = MARKER[flight_id][target_id]['marker'][i - 1]['state']
                    azi = MARKER[flight_id][target_id]['marker'][i - 1]['azimuth']
                    dst = MARKER[flight_id][target_id]['marker'][i - 1]['distance']
                    ts = MARKER[flight_id][target_id]['marker'][i - 1]['timestamp']
                    return {"status": "Ok",
                            "marker_id": marker_id,
                            "state": state,
                            "azimuth": azi,
                            "distance": dst,
                            'timestamp': ts,
                            "llh": pos}, 200
            return {"status": "Error",
                    "message": "Marker {marker_id} not exists".format(marker_id=marker_id)}, 400
        else:
            return {"status": "Error",
                    "message": "Target {target_id} not exists".format(target_id=target_id)}, 400
    else:
        return {"status": "Error",
                "message": "Flight {flight_id} not exists".format(flight_id=flight_id)}, 400


def update(flight_id, target_id, marker_id, state=None):
    """marker_update

    Update marker data &amp; state for specified &#x60;marker_id&#x60;

    :param flight_id: Flight identifier
    :type flight_id: str
    :param target_id: Target identifier
    :type target_id: str
    :param marker_id: Marker identifier
    :type marker_id: str
    :param state: marker state and position
    :type state: dict | bytes
    """

    if flight_id in MARKER and flight_id is not None:
        if target_id in MARKER[flight_id]:
            for i in range(len(MARKER[flight_id][target_id]['marker']), 0, -1):
                if marker_id == MARKER[flight_id][target_id]['marker'][i - 1]['marker_id']:
                    #ToDo Validate marker state
                    new_state = state.get('state', None)
                    new_pos = state.get('llh', None)
                    new_azi = state.get('azimuth', None)
                    new_dst = state.get('distance', None)
                    MARKER[flight_id][target_id]['marker'][i - 1]['llh']=new_pos
                    MARKER[flight_id][target_id]['marker'][i - 1]['state'] = new_state
                    MARKER[flight_id][target_id]['marker'][i - 1]['azimuth'] = new_azi
                    MARKER[flight_id][target_id]['marker'][i - 1]['distance'] = new_dst
                    return {"status": "Ok"}, 200
            return {"status": "Error",
                    "message": "Marker {marker_id} not exists".format(marker_id=marker_id)}, 400
        else:
            return {"status": "Error",
                    "message": "Target {target_id} not exists".format(target_id=target_id)}, 400
    else:
        return {"status": "Error",
                "message": "Flight {flight_id} not exists".format(flight_id=flight_id)}, 400

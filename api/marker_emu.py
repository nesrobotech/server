import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(("192.168.0.22", 8888))
cmd = s.recv(128)
if cmd == b'GET_IP':
    s.send(b'192.168.0.13')
cmd = s.recv(128)
cmd = cmd.decode()
if "SET_PORT:" in cmd:
    port = cmd.split(":")[1]
    s.send(str.encode(port))
cmd = s.recv(128)
cmd = cmd.decode()
if  cmd == "CLOSE":
    s.send(str.encode("CLOSED"))
print(cmd)
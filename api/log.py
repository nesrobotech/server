#ToDo Replace HTTP Status Codes acconding to the https://opensource.zalando.com/restful-api-guidelines/#150

from datetime import datetime


def get_timestamp():
    return datetime.now().strftime("%Y-%m-%d %H:%M:%S")


Log = [
        {
            "timestamp": get_timestamp(),
            "sender": "api_server",
            "message": "api server started"
        }
        ]


def read(lastsync, flight_id=None):
    """log_read

    Read log file #

    :param lastsync: Last sync timestamp
    :type lastsync: str
    :param flight_id: Flight identifier
    :type flight_id: str
    """

    try:
        datetime.strptime(lastsync, "%Y-%m-%d %H:%M:%S")
    except ValueError:
        return {"status": "Error",
                "message": "lastsync format error"}, 400

    loglist = []
    for i in range(len(Log), 0, -1):
        if lastsync < Log[i-1]['timestamp']:
            if flight_id is None:
                loglist.append(Log[i-1])
            else:
                # filter by `flight_id`
                try:
                    if Log[i-1]['flight_id'] == flight_id:
                        loglist.append(Log[i-1])
                except KeyError:
                    pass
        if lastsync >= Log[i-1]['timestamp']:
            break
    return {"status": "Ok",
            "log": loglist}, 200


def save(log_msg):
    """log_read

    Read log file #

    :param log_msg: Log message
    :type log_msg: dict
    :param flight_id: Flight identifier
    :type flight_id: str
    """

    log_msg['timestamp'] = get_timestamp()
    Log.append(log_msg)

    return {"status": "Ok"}, 200

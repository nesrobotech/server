#!/bin/bash
# Linux FAI app & RTLS API server setup v2.1
ORANGE='\e[38;5;208m'
BLUE='\e[38;5;38m'
LIGHTGREY='\e[38;5;253m'
NC='\e[m'

printf  "${ORANGE} \n
        ${ORANGE}000000000000${LIGHTGREY}________${ORANGE}0000${LIGHTGREY}________${ORANGE}000000000000${LIGHTGREY}\n
        ${ORANGE}000000000000${LIGHTGREY}______${ORANGE}00000000${LIGHTGREY}______${ORANGE}000000000000${LIGHTGREY}\n
        ${ORANGE}0000${LIGHTGREY}____________${ORANGE}0000${LIGHTGREY}____${ORANGE}0000${LIGHTGREY}________${ORANGE}0000${LIGHTGREY}____\n
        ${ORANGE}0000${LIGHTGREY}____________${ORANGE}0000${LIGHTGREY}____${ORANGE}0000${LIGHTGREY}________${ORANGE}0000${LIGHTGREY}____\n
        ${ORANGE}00000000000${LIGHTGREY}_____${ORANGE}000000000000${LIGHTGREY}________${ORANGE}0000${LIGHTGREY}____\n
        ${ORANGE}00000000000${LIGHTGREY}_____${ORANGE}000000000000${LIGHTGREY}________${ORANGE}0000${LIGHTGREY}____\n
        ${ORANGE}0000${LIGHTGREY}____________${ORANGE}0000${LIGHTGREY}____${ORANGE}0000${LIGHTGREY}________${ORANGE}0000${LIGHTGREY}____\n
        ${ORANGE}0000${LIGHTGREY}____________${ORANGE}0000${LIGHTGREY}____${ORANGE}0000${LIGHTGREY}________${ORANGE}0000${LIGHTGREY}____\n
        ${ORANGE}0000${LIGHTGREY}____________${ORANGE}0000${LIGHTGREY}____${ORANGE}0000${LIGHTGREY}____${ORANGE}000000000000${LIGHTGREY}\n
        ${ORANGE}0000${LIGHTGREY}____________${ORANGE}0000${LIGHTGREY}____${ORANGE}0000${LIGHTGREY}____${ORANGE}000000000000${NC}\n"

echo  "Install FAI app"
echo $(sudo apt-get install -y curl)
echo $(sudo apt-get install -y python3)
echo $(sudo apt install -y python3-pip)
echo $( sudo apt-get install -y python3-dev)
echo $( sudo apt-get install -y build-essential python3-dev libffi-dev libssl-dev)
echo $( sudo apt-get update && apt-get install -y gcc)
echo $( sudo apt-get -f install)
echo $(sudo apt-get install -y git)
echo $(sudo apt-get install  -y wget)
echo $(sudo pip3 install --upgrade pip)
echo  "Install nodejs"
apt-get update

curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
nvm install 9

#file walker
path="/home/$USER"
cd $path
pwd

#remove existed folder
rm -rf ./RTLS_server

#create project folder
mkdir RTLS_server
cd ./RTLS_server

#git clone
git clone "https://github.com/bogdan-molodets/fai.git"

echo "FAI app was installed."
cd ./fai
echo "Install dependencies for FAI app..."
nvm use 9
npm install
npm run build

echo "Dependencies were installed."

printf  "${ORANGE} Install RTLS API server\n"
cd ..
echo $(pip3 install --user cython)
echo $(pip3 install --user re)
echo $(pip3 install --user sys)
echo $(pip3 install --user subprocess)
echo $(pip3 install --user argparse)
echo $(pip3 install --user requests)
echo $(pip3 install --user json)
echo $(pip3 install --user flask)
echo $(pip3 install --user connexion)
echo $(pip3 install --user socket)
echo $(pip3 install --user flask_cors)
echo $(pip3 install --user "connexion[swagger-ui]")
echo $(pip3 install --user pyproj)

git clone https://bitbucket.org/nesrobotech/server.git
cd server/rtkbin
sudo cp convbin /usr/local/bin/
sudo cp rnx2rtkp /usr/local/bin/
sudo cp rtkrcv /usr/local/bin/
sudo cp str2str /usr/local/bin/
